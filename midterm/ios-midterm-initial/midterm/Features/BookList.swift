import SwiftUI
import SwiftData

struct BookList: View {
  
  var body: some View {
    VStack {
      Text("Your book list will go here")
    }
    .navigationTitle("Books - yourdukeid")
  }
}

#Preview {
  let preview = PreviewContainer([Book.self])
  preview.add(items: Book.previewData)
  return
    BookList()
      .modelContainer (preview.container)
}
