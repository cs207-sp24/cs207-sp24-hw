import SwiftUI
import SwiftData

struct ReadingList: View {

  var body: some View {
    VStack(spacing: 3) {
      Text("Your reading list will be here.")
      Text("Remember, the developer always wins.")
        .padding(.top, 20)
        .font(.headline)
      Text("You got this.")
        .font(.caption)
    }
  }
}

#Preview {
  let preview = PreviewContainer([Book.self])
  preview.add(items: Book.previewData)
  return
    ReadingList()
      .modelContainer (preview.container)
}
