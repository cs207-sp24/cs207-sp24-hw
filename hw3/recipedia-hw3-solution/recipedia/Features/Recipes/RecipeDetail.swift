import SwiftUI

struct RecipeDetail: View {
  @Bindable var recipe: Recipe
  @State var scale: Double = 1.0

  var body: some View {
    ScrollView {
      VStack {
        recipeImage
        Text(recipe.name).font(.headline)
        Button(action: {
          if recipe.lastPreparedAt != nil {
            recipe.lastPreparedAt = nil
          } else {
            recipe.lastPreparedAt = Date.now
          }}) {
            HStack(spacing: 4) {
              Text("Previously Prepared")
              recipe.lastPreparedAt != nil ? Image(systemName: "checkmark.circle") : Image(systemName: "circle")
            }
        }
        Text(cuisineString())
        HStack {
          Text("Scale the recipe")
            .font(.headline)
          Picker("Scale the recipe", selection: $scale) {
            Text("0.5").tag(0.5)
            Text("1").tag(1.0)
            Text("2").tag(2.0)
          }
          .pickerStyle(.menu)
        }
        RecipeIngredientsInstructions(recipe: recipe, scale: scale)
        Text("Notes")
          .font(.headline)
        TextEditor(text: $recipe.notes)
          .frame(height: 200)
      }
      .padding(12)
      Spacer()
    }
  }

  func cuisineString() -> String {
    switch recipe.cuisine {
    case .american: "American"
    case .chinese(let region): "Chinese - \(region)"
    case .japanese: "Japanese"
    case .italian(let pairing): "Italian, pair with a \(pairing)"
    }
  }

  var recipeImage: some View {
    AsyncImage(url: recipe.thumbnailUrl) { image in
      image
        .resizable()
        .aspectRatio(contentMode: .fit)
        .frame(height: 200)
    } placeholder: {
      if recipe.thumbnailUrl != nil {
        ProgressView()
      } else {
        Image(systemName: "fork.knife")
      }
    }
  }
}

#Preview {
  RecipeDetail(recipe: Recipe.previewData[3])
}

struct RecipeIngredientsInstructions: View {
  let recipe: Recipe
  let scale: Double

  var body: some View {
    VStack(alignment: .leading) {
      ForEach(recipe.sectionLabels, id: \.self) { sectionLabel in
        Text(sectionLabel)
          .bold()
          .padding(.top, 6)
        ForEach(recipe.ingredientsForSectionLabel(sectionLabel)) { ingredient in
          Text(ingredientDisplay(ingredient))
        }
      }
      if recipe.unsectionedIngredients.count > 0 && recipe.sectionLabels.count > 0 {
        Text("Other")
          .bold()
          .padding(.top, 6)
      }
      ForEach(recipe.unsectionedIngredients) { ingredient in
        Text(ingredientDisplay(ingredient))
      }
      ForEach(recipe.instructions.sorted(by: { $0.position < $1.position })) { instruction in
        Text(instruction.instructionText)
          .padding(.top, 6)
      }
    }
  }

  func ingredientDisplay(_ recipeIngredient: RecipeIngredient) -> String {
    var returnString = ""
    if let quantity = recipeIngredient.quantity {
      returnString.append(String(quantity * scale) + " ")
    }
    if let unit = recipeIngredient.unit {
      returnString.append(String(unit) + " ")
    }
    returnString.append(recipeIngredient.ingredient.name)
    return returnString
  }

  func ingredientDisplayAdvanced(_ recipeIngredient: RecipeIngredient) -> String {
    [
      recipeIngredient.quantity.map { String($0) } ?? "",
      recipeIngredient.unit.map { String($0) } ?? "",
      recipeIngredient.ingredient.name
    ]
      .filter { !$0.isEmpty }
      .joined(separator: " ")

  }
}
