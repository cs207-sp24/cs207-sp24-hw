import SwiftUI

@main
struct recipediaApp: App {
    var body: some Scene {
        WindowGroup {
            RecipeList()
        }
    }
}
